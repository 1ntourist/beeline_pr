from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.postgres.search import SearchVector
from .models import Contact
from .forms import SearchForm


def phone_list(request):
    # phone_list = Contact.objects.all()
    object_list = Contact.objects.all().order_by('first_name', 'last_name', 'third_name')
    paginator = Paginator(object_list, 5)
    page = request.GET.get('page')
    try:
        contacts = paginator.page(page)
    except PageNotAnInteger:
        contacts = paginator.page(1)
    except EmptyPage:
        contacts = paginator.page(paginator.num_pages)

    return render(request, 'phone_book/phone_list.html', {'page': page,
                                                          'contacts': contacts})


def contact_detail(request, slug):
    contact = get_object_or_404(Contact, slug=slug)
    return render(request, 'phone_book/contact_detail.html', {'contact': contact})


def contact_search(request):
    form = SearchForm()
    query = None
    result = []
    if 'query' in request.GET:
        form = SearchForm(request.GET)
    if form.is_valid():
        query = form.cleaned_data['query']
        result = Contact.objects.annotate(
            search=SearchVector('last_name', 'first_name', 'third_name', 'phone_number'),
        ).filter(search=query)
    return render(request, 'phone_book/search.html', {'form': form, 'query': query, 'result': result})