from django.contrib import admin
from .models import Contact


@admin.register(Contact)
class PhoneNumberAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'third_name', 'slug')
    list_filter = ('created',)
    prepopulated_fields = {'slug': ('phone_number',)}