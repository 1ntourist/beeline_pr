from django.urls import path
from . views import *

app_name = 'phone_book'


urlpatterns =[
    path('', phone_list, name='phone_list'),
    path('contacts/<slug:slug>/', contact_detail, name='contact_detail'),
    path('search/', contact_search, name='contact_search'),
]