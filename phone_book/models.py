from django.db import models
from django.core.validators import RegexValidator
from django.urls import reverse

phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                             message="Phone number must be entered in the format: '+996777777777'. Up to 15 digits allowed.")


class Contact(models.Model):
    first_name = models.CharField(max_length=20, verbose_name='Фамилия')
    last_name = models.CharField(max_length=20, verbose_name='Имя')
    third_name = models.CharField(max_length=20, null=True, blank=True, verbose_name='Отчество')
    date_of_birth = models.DateField(null=True, blank=True, verbose_name='Дата рождения')
    phone_number = models.CharField(validators=[phone_regex], max_length=13, blank=True, verbose_name='Номер телефона',
                                    unique=True, )
    created = models.DateTimeField(auto_now_add=True, verbose_name='Создан')
    updated = models.DateTimeField(auto_now=True, verbose_name='Изменен')
    slug = models.SlugField(max_length=13, unique=True)

    def get_birth_year(self):
        return self.date_of_birth.year

    class Meta:
        verbose_name = "Контакт"
        verbose_name_plural = "Контакты"

    def __str__(self):
        if self.third_name:
            return f'{self.first_name} {self.last_name} {self.third_name}'
        else:
            return f'{self.first_name} {self.last_name}'

    def get_absolute_url(self):
        return reverse('phone_book:contact_detail', self.slug)